<?php

namespace App\Http\Controllers;

use App\Jobs\CheckTriggerJob;
use App\Models\Leed;
use App\Models\Trigger;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;

class AnalyticsController extends Controller
{

    public function report(Request $request)
    {
        $id = $request->get('id');
        $session = $request->get('an_key');
        $event = $request->get('event');
        $leed = Leed::where('session', $session)->where('user_id', $id)->first();

        if (is_null($leed)) {
            $leed = Leed::create([
                'session' => $session,
                'user_id' => $id
            ]);
        }

        $event = $leed->events()->create([
            'type' => isset($event['type']) ? $event['type'] : null,
            'element_class' => isset($event['className']) ? $event['className'] : null,
            'element_id' => isset($event['id']) ? $event['id'] : null,
            'element_tag' => isset($event['tagName']) ? $event['tagName'] : null,
            'timestamp' => Carbon::parse($event['timeStamp'] * 1000),
            'url' => $event['baseURI'],
            'value' => $event['value']
        ]);

        CheckTriggerJob::dispatch($event);
    }
}
