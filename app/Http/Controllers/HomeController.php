<?php

namespace App\Http\Controllers;

use App\Models\DoneBlock;
use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {

        $start = Carbon::now()->subDays(7);
        $eventsCount = Event::whereHas('leed', function ($query) {
            $query->where('user_id', auth()->user()->id);
        })->where('created_at', '>', $start)->count();

        $eventsCount = Event::whereHas('leed', function ($query) {
            $query->where('user_id', auth()->user()->id);
        })->where('created_at', '>', $start)->count();

        $actionsCount = DoneBlock::whereHas('leed', function ($query) {
            $query->where('user_id', auth()->user()->id);
        })->where('created_at', '>', $start)->count();

        $leeds = auth()->user()->leeds()->where('created_at', '>', $start)->count();

        $leedsWithEmail = auth()->user()->leeds()->where('created_at', '>', $start)->whereNotNull('email')->count();


        return view('home', compact('eventsCount', 'actionsCount', 'leeds', 'leedsWithEmail'));
    }
}
