<?php

namespace App\Http\Controllers\Lists;

use App\Http\Controllers\Controller;
use App\Models\Leed;
use Illuminate\Http\Request;

class LeedsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $perPage = $request->get('per_page', 25);
        $list = auth()->user()->lists()->where('id', $id)->firstOrFail();
        $leeds = $list->leeds()->orderBy('id', 'desc')->paginate($perPage);
        return view('lists.leeds.index', compact('list', 'leeds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $list = auth()->user()->lists()->where('id', $id)->first();
        $tags = auth()->user()->tags;
        return view('lists.leeds.create', compact('list', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $listId)
    {
        $this->validate($request, [
            'email' => 'nullable|email'
        ]);
        $data = $request->all();

        $list = auth()->user()->lists()->where('id', $listId)->first();
        $leed = auth()->user()->leeds()->create($data);
        $list->leeds()->attach($leed->id);
        $leed->tags()->sync($request->get('tags', []));

        return redirect()->route('lists.leeds.index', $list->id)->with('success', 'Kontakt dodany pomyślnie');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($list, $id)
    {
        $list = auth()->user()->lists()->where('id', $list)->firstOrFail();
        $leed = $list->leeds()->where('id', $id)->firstOrFail();
        $tags = auth()->user()->tags;

        return view('lists.leeds.edit', compact('list', 'leed', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $list, $id)
    {
        $this->validate($request, [
            'email' => 'nullable|email'
        ]);

        $list = auth()->user()->lists()->where('id', $list)->firstOrFail();
        $leed = $list->leeds()->where('id', $id)->firstOrFail();
        $leed->tags()->sync($request->get('tags', []));
        $leed->update($request->all());
        return redirect()->route('lists.leeds.index', $list->id)->with('success', 'Kontakt został zmieniony');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($list, $id)
    {
        $list = auth()->user()->lists()->where('id', $list)->firstOrFail();
        $leed = $list->leeds()->detach($id);
        return redirect()->route('lists.leeds.index', $list->id)->with('success', 'Kontakt został usunięty z tej listy');
    }
}
