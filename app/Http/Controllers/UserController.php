<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function edit()
    {
        $user = auth()->user();

        return view('user.edit', compact('user'));
    }

    public function update(Request $request)
    {

        $user = auth()->user();

        $this->validate($request, [
            'email' => 'required|email|unique:users,email,'.$user->id,
            'password' => 'nullable|min:8|confirmed'
        ]);


        $data = $request->all();
        if ($request->has('password')) {
            $data['password'] = bcrypt($data['password']);
        }


        $user->update($data);

        return redirect()->back()->with('success', 'Dane zostały zmienione');
    }
}
