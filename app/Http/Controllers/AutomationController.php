<?php

namespace App\Http\Controllers;

use App\Models\Trigger;
use Database\Seeders\AutomotionSeeder;
use Illuminate\Http\Request;

class AutomationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = auth()->user()->tags;
        $lists = auth()->user()->lists;

        return view('automation.create', compact('tags', 'lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $schema = json_encode($request->get('schema'));
        $name = $request->get('name');
        $automation = auth()->user()->automotions()->create(['name' => $name, 'schema' => $schema]);
        return $automation;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sequence = auth()->user()->automotions()->where('id', $id)->firstOrFail();
        $sequence = $sequence->schema;
        $tags = auth()->user()->tags;
        $lists = auth()->user()->lists;
        return view('automation.show', compact('sequence', 'tags', 'lists'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $automation = auth()->user()->automotions()->where('id', $id)->firstOrFail();

        $sequence = $automation->schema;
        $tags = auth()->user()->tags;
        $lists = auth()->user()->lists;
        return view('automation.edit', compact('automation', 'sequence', 'tags', 'lists'));
    }


    public function update(Request $request, $id)
    {
        $automation = auth()->user()->automotions()->where('id', $id)->firstOrFail();
        $schema = json_encode($request->get('schema'));
        $name = $request->get('name');
        $automation->update(['name' => $name, 'schema' => $schema]);
        return $automation->fresh();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        auth()->user()->automotions()->where('id', $id)->firstOrFail()->delete();
    }
}
