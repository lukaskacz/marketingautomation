<?php

namespace App\Models;

use Database\Seeders\UserSeeder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Automotion extends Model
{
    protected $fillable = ['name', 'schema'];
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
