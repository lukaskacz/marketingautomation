<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoneBlock extends Model
{
    use HasFactory;

    protected $fillable = ['leed_id', 'block_id'];

    public function leed()
    {
        return $this->belongsTo(Leed::class);
    }
}
