<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['type', 'value', 'element_id', 'element_tag', 'element_class', 'url', 'details', 'leed_id', 'mailing_list_id','timestamp'];
    use HasFactory;

    public function leed()
    {
        return $this->belongsTo(Leed::class);
    }
}
