<?php

namespace App\Models;

use App\Events\OnSubscribe;
use App\Events\OnUnsubscribe;
use Illuminate\Database\Eloquent\Relations\Pivot;

class LeedMailingList extends Pivot
{
    public static function boot()
    {
        parent::boot();

        static::saving(function ($item) {
            
            OnSubscribe::dispatch($item);
        });

        static::deleted(function ($item) {
            OnUnsubscribe::dispatch($item);
        });
    }
}
