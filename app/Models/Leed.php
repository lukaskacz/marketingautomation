<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leed extends Model
{
    use HasFactory;

    protected $fillable = ['email', 'session', 'user_id', 'name', 'custom_field_1', 'custom_field_2', 'custom_field_3', 'custom_field_4'];



    public function lists()
    {
        return $this->belongsToMany(MailingList::class)->using(LeedMailingList::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function done_blocks()
    {
        return $this->hasMany(DoneBlock::class);
    }
}
