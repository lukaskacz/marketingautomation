<?php

namespace App\Listeners;

use App\Events\OnUnsubscribe;
use App\Jobs\CheckTriggerJob;
use App\Models\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Carbon\Carbon;

class OnUnsubscribeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnUnsubscribe  $event
     * @return void
     */
    public function handle(OnUnsubscribe $event)
    {
        $e = Event::create([
            'type' => 'unsubscribe',
            'mailing_list_id' => $event->getItem()->mailing_list_id,
            'leed_id' => $event->getItem()->leed_id,
            'timestamp' => Carbon::now()
        ]);

        CheckTriggerJob::dispatch($e);
    }
}
