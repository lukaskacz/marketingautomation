<?php

namespace App\Listeners;

use App\Events\OnRemoveTag;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OnRemoveListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnRemoveTag  $event
     * @return void
     */
    public function handle(OnRemoveTag $event)
    {
        //
    }
}
