<?php

namespace App\Listeners;

use App\Events\OnTag;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OnTagListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnTag  $event
     * @return void
     */
    public function handle(OnTag $event)
    {
        //
    }
}
