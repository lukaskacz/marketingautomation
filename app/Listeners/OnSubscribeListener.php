<?php

namespace App\Listeners;

use App\Events\OnSubscribe;
use App\Jobs\CheckTriggerJob;
use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OnSubscribeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnSubscribe  $event
     * @return void
     */
    public function handle(OnSubscribe $event)
    {

        $e = Event::create([
            'type' => 'subscribe',
            'mailing_list_id' => $event->getItem()->mailing_list_id,
            'leed_id' => $event->getItem()->leed_id,
            'timestamp'=>Carbon::now()
        ]);
        CheckTriggerJob::dispatch($e);
    }
}
