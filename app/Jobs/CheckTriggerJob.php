<?php

namespace App\Jobs;

use App\Models\Automotion;
use App\Models\Event;
use App\Models\MailingList;
use App\Models\User;
use App\Notifications\SendMail;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckTriggerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $event;
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $automotions = Automotion::where('user_id', $this->event->leed->user_id)->get();

        foreach ($automotions as $automotion) {
            $block = json_decode($automotion->schema);
            $events = $this->event->leed->events;

            $this->doAction($block, $events, $automotion->created_at, $automotion);
        }
    }

    private function findEvents($type, $events, $fromDate)
    {
        $events = $events->where('type', $type)->where('created_at', '>=', $fromDate);
        return $events;
    }
    private function doAction($block, $events, $lastEventDate = null, $automotion)
    {
        try {
            if (!$lastEventDate) {
                $lastEventDate = $events->first()->created_at;
            }
            if ($block) {
                if ($block->block_type == 'trigger' || $block->block_type == 'condition') {
                    if ($block->type == 'load') {
                        $loadEvents = $this->findEvents('load', $events, $lastEventDate);
                        foreach ($loadEvents as $event) {
                            $flag = true;
                            if (isset($block->url) && $block->url && strpos($event->url, $block->url) === false) {
                                $flag = false;
                            }

                            if (isset($block->time) && $block->time) {
                                $nextUnload = $this->findEvents('unload', $events, $lastEventDate)->first();
                                $nextLoad = $this->findEvents('load', $events, $lastEventDate)->first();

                                $next = null;
                                $time = 0;
                                if ($nextUnload) {
                                    $next = $nextUnload;
                                }
                                if ($nextLoad) {
                                    $next = $nextLoad;
                                }
                                if ($next) {
                                    $time = Carbon::parse($event->created_at)->diffInSeconds($next->created_at);
                                }

                                if ($time < $block->time) {
                                    $flag = false;
                                }
                            }

                            if ($flag) {
                                if ($block->block_type == 'trigger') {
                                    return $this->doAction($block->next, $events, $event->created_at, $automotion);
                                } else {
                                    return $this->doAction($block->success_next, $events, $event->created_at, $automotion);
                                }
                            }
                        }
                        if ($block->block_type == 'condition') {
                            return $this->doAction($block->fail_next, $events, $lastEventDate, $automotion);
                        }
                    }

                    if ($block->type == 'click') {
                        $clickEvents = $this->findEvents('click', $events, $lastEventDate);
                        foreach ($clickEvents as $event) {
                            $flag = true;
                            if (isset($block->url) && $block->url && strpos($event->url, $block->url) === false) {
                                $flag = false;
                            }

                            if (isset($block->element_tag) && $block->element_tag && strpos($event->element_tag, $block->element_tag) === false) {
                                $flag = false;
                            }

                            if (isset($block->element_id) && $block->element_id && strpos($event->element_id, $block->element_id) === false) {
                                $flag = false;
                            }

                            if (isset($block->element_class) && $block->element_class && strpos($event->element_class, $block->element_class) === false) {
                                $flag = false;
                            }
                            if ($flag) {
                                if ($block->block_type == 'trigger') {
                                    return $this->doAction($block->next, $events, $event->created_at, $automotion);
                                } else {
                                    return $this->doAction($block->success_next, $events, $event->created_at, $automotion);
                                }
                            }
                        }
                        if ($block->block_type == 'condition') {
                            return $this->doAction($block->fail_next, $events, $lastEventDate, $automotion);
                        }
                    }

                    if ($block->type == 'change') {
                        $changeEvents = $this->findEvents('change', $events, $lastEventDate, $automotion);
                        foreach ($changeEvents as $event) {

                            $flag = true;
                            if (isset($block->element_tag) && $block->element_tag && strpos($event->element_tag, $block->element_tag) === false) {
                                $flag = false;
                            }

                            if (isset($block->element_id) && $block->element_id && strpos($event->element_id, $block->element_id) === false) {
                                $flag = false;
                            }

                            if (isset($block->element_class) && $block->element_class && strpos($event->element_class, $block->element_class) === false) {
                                $flag = false;
                            }
                            if ($flag) {
                                if ($block->block_type == 'trigger') {
                                    return $this->doAction($block->next, $events, $event->created_at, $automotion);
                                } else {
                                    return $this->doAction($block->success_next, $events, $event->created_at, $automotion);
                                }
                            }
                        }
                        if ($block->block_type == 'condition') {
                            return $this->doAction($block->fail_next, $events, $lastEventDate, $automotion);
                        }
                    }

                    if ($block->type == 'subscribe') {

                        $subscribeEvents = $this->findEvents('subscribe', $events, $lastEventDate);

                        foreach ($subscribeEvents as $event) {
                            $flag = true;
                            if (isset($block->mailing_list_id) && $block->mailing_list_id && intval($block->mailing_list_id) !== $event->mailing_list_id) {
                                $flag = false;
                            }


                            if ($flag) {
                                if ($block->block_type == 'trigger') {
                                    return $this->doAction($block->next, $events, $event->created_at, $automotion);
                                } else {
                                    return $this->doAction($block->success_next, $events, $event->created_at, $automotion);
                                }
                            }
                        }

                        if ($block->block_type == 'condition') {
                            return $this->doAction($block->fail_next, $events, $lastEventDate, $automotion);
                        }
                    }

                    if ($block->type == 'unsubscribe') {
                        $unsubscribeEvents = $this->findEvents('unsubscribe', $event, $lastEventDate);
                        foreach ($unsubscribeEvents as $event) {
                            $flag = true;
                            if (isset($block->mailing_list_id) && $block->mailing_list_id && $block->mailing_list_id !== $event->mailing_list_id) {
                                $flag = false;
                            }

                            if ($flag) {
                                if ($block->block_type == 'trigger') {
                                    return $this->doAction($block->next, $events, $event->created_at, $automotion);
                                } else {
                                    return $this->doAction($block->success_next, $events, $event->created_at, $automotion);
                                }
                            }
                        }

                        if ($block->block_type == 'condition') {
                            return $this->doAction($block->fail_next, $events, $lastEventDate, $automotion);
                        }
                    }


                    if ($block->type == 'tag') {
                        $tagEvents = $this->findEvents('tag', $event, $lastEventDate);
                        foreach ($tagEvents as $event) {
                            $flag = true;
                            if (isset($block->tag_id) && $block->tag_id && $block->tag_id !== $event->tag_id) {
                                $flag = false;
                            }

                            if ($flag) {
                                if ($block->block_type == 'trigger') {
                                    return $this->doAction($block->next, $events, $event->created_at, $automotion);
                                } else {
                                    return $this->doAction($block->success_next, $events, $event->created_at, $automotion);
                                }
                            }
                        }

                        if ($block->block_type == 'condition') {
                            return $this->doAction($block->fail_next, $events, $lastEventDate, $automotion);
                        }
                    }

                    if ($block->type == 'remove_tag') {
                        $removeTagEvents = $this->findEvents('remove_tag', $event, $lastEventDate);
                        foreach ($removeTagEvents as $event) {
                            $flag = true;
                            if (isset($block->tag_id) && $block->tag_id && $block->tag_id !== $event->tag_id) {
                                $flag = false;
                            }

                            if ($flag) {
                                if ($block->block_type == 'trigger') {
                                    return $this->doAction($block->next, $events, $event->created_at, $automotion);
                                } else {
                                    return $this->doAction($block->success_next, $events, $event->created_at, $automotion);
                                }
                            }
                        }

                        if ($block->block_type == 'condition') {
                            return $this->doAction($block->fail_next, $events, $lastEventDate, $automotion);
                        }
                    }
                }

                if ($block->block_type == 'action') {
                    if ($block->type == 'subscribe') {
                        if (!$this->blockIsDone($block, $events[0]->leed)) {
                            $leed = $events[0]->leed;
                            $leed->lists()->syncWithoutDetaching($block->mailing_list_id);
                        }
                        return $this->doAction($block->next, $events, $lastEventDate, $automotion);
                    }

                    if ($block->type == 'unsubscribe') {
                        if (!$this->blockIsDone($block, $events[0]->leed)) {
                            $leed = $events[0]->leed;
                            $leed->lists()->detach($block->mailing_list_id);
                        }
                        return $this->doAction($block->next, $events, $lastEventDate, $automotion);
                    }

                    if ($block->type == 'tag') {
                        if (!$this->blockIsDone($block, $events[0]->leed)) {
                            $leed = $events[0]->leed;
                            $leed->tags()->syncWithoutDetaching($block->tag_id);
                        }
                        return $this->doAction($block->next, $events, $lastEventDate, $automotion);
                    }


                    if ($block->type == 'remove_tag') {
                        if (!$this->blockIsDone($block, $events[0]->leed)) {
                            $leed = $events[0]->leed;
                            $leed->tags()->detach($block->tag_id);
                        }
                        return $this->doAction($block->next, $events, $lastEventDate, $automotion);
                    }

                    if ($block->type == 'wait') {
                        $date = Carbon::parse($lastEventDate);
                        if ($block->interval_type == 'minute') {
                            $date = $date->addMinutes($block->interval);
                        }
                        if ($block->interval_type == 'hours') {
                            $date = $date->addHours($block->interval);
                        }

                        if ($block->interval_type == 'days') {
                            $date = $date->addDays($block->interval);
                        }

                        return $this->doAction($block->next, $events, $date->format('Y-m-d H:i:s'), $automotion);
                    }

                    if ($block->type == 'send') {
                        if (!$this->blockIsDone($block, $events[0]->leed)) {
                            $leed = $events[0]->leed->fresh();

                            if ($leed->email) {
                                $user = new User(['email' => $leed->email]);
                                $user->notify(new SendMail($block->subject, $block->content));
                            }
                        }
                        return $this->doAction($block->next, $events, $lastEventDate, $automotion);
                    }

                    if ($block->type == 'update') {

                        $allChangeEvents = $events->first()->leed->events->where('created_at', '>=', $automotion->created_at)->where('value', '!=', '')->sortByDesc('created_at');
                        if (!$this->blockIsDone($block, $events[0]->leed)) {
                            foreach ($allChangeEvents as $event) {
                                $flag = true;
                                if (isset($block->url) && $block->url && strpos($event->url, $block->url) === false) {
                                    $flag = false;
                                }

                                if (isset($block->element_tag) && $block->element_tag && strpos($event->element_tag, $block->element_tag) === false) {
                                    $flag = false;
                                }

                                if (isset($block->element_id) && $block->element_id && strpos($event->element_id, $block->element_id) === false) {
                                    $flag = false;
                                }

                                if (isset($block->element_class) && $block->element_class && strpos($event->element_class, $block->element_class) === false) {
                                    $flag = false;
                                };
                                if ($flag) {
                                    $event->leed->update([$block->field => $event->value]);
                                    return $this->doAction($block->next, $events, $lastEventDate, $automotion);
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    private function blockIsDone($block, $leed)
    {
        $isDone = $leed->done_blocks->where('block_id', $block->id)->count() > 0;

        if (!$isDone) {
            $leed->done_blocks()->create(['block_id' => $block->id]);
        }
        return $isDone;
    }
}
