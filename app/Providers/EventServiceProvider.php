<?php

namespace App\Providers;

use App\Events\OnSubscribe;
use App\Events\OnUnsubscribe;
use App\Listeners\OnSubscribeListener;
use App\Listeners\OnUnsubscribeListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        OnSubscribe::class => [
            OnSubscribeListener::class
        ],
        OnUnsubscribe::class => [
            OnUnsubscribeListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
