<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    public function boot(Dispatcher $events)
    {

        Paginator::useBootstrap();

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {


            $event->menu->add([
                'text'    => 'Tagi',
                'icon'    => 'fas fa-fw fa-envelope',
                'url' => route('tags.index')
            ]);

            $lists = auth()->user()->lists;
            $submenus = [];
            foreach ($lists as $list) {
                $submenus[] = [
                    'text' => $list->name,
                    'url' => route('lists.leeds.index', $list->id),
                    'icon' => ''
                ];
            }

            $submenus[] = [
                'text' => 'Dodaj',
                'url' => route('lists.create'),
                'icon' => 'fas fa-plus'
            ];

            $event->menu->add([
                'text'    => 'Listy mailingowe',
                'icon'    => 'fas fa-fw fa-envelope',
                'label'       => $lists->count(),
                'label_color' => 'success',
                'submenu' => $submenus
            ]);


            $automotions = auth()->user()->automotions;

            $submenus = [];
            foreach ($automotions as $automotion) {
                $submenus[] = [
                    'text' => $automotion->name,
                    'url' => route('automation.edit', $automotion->id),
                    'icon' => ''
                ];
            }

            $submenus[] = [
                'text' => 'Dodaj',
                'url' => route('automation.create'),
                'icon' => 'fas fa-plus'
            ];


            $event->menu->add([
                'text'    => 'Automatyzacja',
                'icon'    => 'fas fa-fw fa-robot',
                'label'       => $automotions->count(),
                'label_color' => 'success',
                'submenu' => $submenus

            ]);

            $event->menu->add([
                'text' => 'Moje konto',
                'url'  => route('user.edit'),
                'icon' => 'fas fa-fw fa-user',
            ]);
        });
    }
}
