<?php

namespace Database\Factories;

use App\Models\Leed;
use Illuminate\Database\Eloquent\Factories\Factory;

class LeedFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Leed::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->email
        ];
    }
}
