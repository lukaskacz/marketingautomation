<?php

namespace Database\Seeders;

use App\Models\MailingList;
use App\Models\User;
use Illuminate\Database\Seeder;

class ListsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::first();
        $user->lists()->create([
            'name' => 'Główna lista'
        ]);

        $user->lists()->create([
            'name' => 'Zarejestrowani'
        ]);

        $user->lists()->create([
            'name' => 'Płacący'
        ]);
    }
}
