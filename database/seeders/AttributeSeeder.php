<?php

namespace Database\Seeders;

use App\Models\MailingList;
use App\Models\Attribute;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = MailingList::all();

        foreach ($lists as $list) {
            Attribute::factory(4)->create(['mailing_list_id' => $list->id]);
        }
    }
}
