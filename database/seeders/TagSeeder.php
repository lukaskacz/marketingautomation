<?php

namespace Database\Seeders;

use App\Models\Leed;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\User;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::first();
        $user->tags()->create([
            'name' => 'Zaangażowani'
        ]);

        $user->tags()->create([
            'name' => 'Niezaangażowani'
        ]);

        $user->tags()->create([
            'name' => 'Mężczyźni'
        ]);

        $user->tags()->create([
            'name' => 'Kobiety'
        ]);
    }
}
