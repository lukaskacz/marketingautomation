<?php

namespace Database\Seeders;

use App\Models\Action;
use App\Models\Condition;
use App\Models\User;
use Illuminate\Database\Seeder;

class SequenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        $list = $user->lists()->first();
        $trigger = $user->triggers()->create([
            'type' => 'tag',
            'tag_id' => 1,
            'time' => 30,
            'name' => 'Przykładowy schemat',
        ]);

        $condition = Condition::create(['type' => 'load', 'url' => 'dziekujemy']);
        $trigger->update([ 'next_id' => $condition->id, 'next_type' => Condition::class]);


        $actionSuccess = Action::create(['type' => 'tag', 'tag_id' => 1]);
        $actionFail = Action::create(['type' => 'subscribe', 'mailing_list_id' => 1]);

        $condition->update(['success_next_id' => $actionSuccess->id, 'success_next_type' => Action::class]);

        $actionSuccess->update(['next_id' => $actionFail->id, 'next_type' => Action::class]);
    }
}
