<?php

namespace Database\Seeders;

use App\Models\AttributeValue;
use App\Models\Leed;
use Illuminate\Database\Seeder;

class AttributeValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $leeds = Leed::all();
        foreach ($leeds as $leed) {
            $list = $leed->list;

            $attributes = $list->attributes;

            foreach ($attributes as $attribute) {
                AttributeValue::factory()->create(['leed_id' => $leed->id, 'attribute_id' => $attribute->id]);
            }
        }
    }
}
