<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AutomotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $automotion = [
            'id' => 1,
            'block_type' => 'trigger',
            'type' => 'subscribe',
            // 'url' => 'lists',
            //  'element_tag' => 'BUTTON',
            // 'element_id' => 'delete',
            // 'element_class' => 'schema-name',
            'next' => [
                'id' => 2,
                'block_type' => 'action',
                'type' => 'send',
                'subject' => 'Witaj',
                'content' => 'test',
                'mailing_list_id' => 1,
                'next' => [
                    'id' => 3,
                    'block_type' => 'condition',
                    'type' => 'load',
                    'url' => 'dziekujemy',
                    'success_next' => [
                        'id' => 4,
                        'block_type' => 'action',
                        'type' => 'subscribe',
                        'mailing_list_id' => 1,
                        'next' => [
                            'id' => 5,
                            'block_type' => 'action',
                            'type' => 'tag',
                            'tag_id' => 1
                        ]
                    ]
                ]
            ]
        ];

        $automotion = User::first()->automotions()->create([
            'name' => 'domyślny schemat',
            'schema' => json_encode($automotion)
        ]);
    }
}
