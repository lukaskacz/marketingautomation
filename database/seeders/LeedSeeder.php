<?php

namespace Database\Seeders;

use App\Models\Leed;
use App\Models\MailingList;
use Illuminate\Database\Seeder;

class LeedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = MailingList::all();
        foreach ($lists as $list) {
            Leed::factory(200)->create(['mailing_list_id' => $list->id]);
        }
    }
}
