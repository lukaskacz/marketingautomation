<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('element_id')->nullable();
            $table->string('element_class')->nullable();
            $table->string('element_tag')->nullable();
            $table->text('value')->nullable();
            $table->text('url')->nullable();
            $table->timestamp('timestamp');
            $table->unsignedBigInteger('leed_id');
            $table->unsignedBigInteger('mailing_list_id')->nullable();
            $table->unsignedBigInteger('tag_id')->nullable();

            $table->foreign('mailing_list_id')
                ->references('id')
                ->on('mailing_lists')
                ->onDelete('cascade');

            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');

            $table->foreign('leed_id')
                ->references('id')
                ->on('leeds')
                ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
