<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoneBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('done_blocks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('leed_id');
            $table->string('block_id');
            $table->timestamps();

            $table->foreign('leed_id')
                ->references('id')
                ->on('leeds')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('done_blocks');
    }
}
