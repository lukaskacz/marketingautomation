<p>W celu zainstalowania aplikacji należy zainstalować na maszynie interpreter PHP w wersji 7.4 oraz skrypt Composer. Instrukcja instalacji dostępna jest pod adresem <a href="https://getcomposer.org/doc/00-intro.md">https://getcomposer.org/doc/00-intro.md</a></p>
<p>W celu minifikacji plików JavaScript należy zainstalować środowisko uruchomieniowe dla Node.js oraz manager pakietów JavaScript NPM. W tym celu należy skorzystać z managera wersji Node.JS - NVM. Instrukcja instalacji znajduje się pod adresem <a href="https://github.com/nvm-sh/nvm">https://github.com/nvm-sh/nvm</a></p>
<p>Aplikacja łączy się z bazą danych MySQL. Maszyna powinna posiadać zainstalowany serwer MySQL. </p>
<p>W celu zainstalowania pakietów PHP oraz node.js należy uruchomić komendę <code>composer install && npm install</code></p>
<p>Kolejnym krokiem jest skopiowanie pliku .env.example i zapisanie pod nazwą .env</p>
<p>W celu poprawngo działania mechanizmów szyfrujących dostarczanych przez framework należy wygenerować klucz aplikacji wpisując komendę php artisan key:generate. Klucz zostanie zapisany w pliku .env. Następnie w pliku .env należy uzupełnić pola odpowiadające komunikacji z bazą danych. Pola te mają nazwę:</p>
<ul>
<li>DB_HOST</li>
<li>DB_PORT</li>
<li>DB_DATABASE</li>
<li>DB_USERNAME</li>
<li>DB_PASSWORD</li>
</ul>
</p>
<p>Konfiguracja mailera polega na uzupełnieniu poniższych pól w pliku .env</p>
<ul>
<li>
<li>MAIL_MAILER</li>
<li>MAIL_HOST</li>
<li>MAIL_PORT</li>
<li>MAIL_USERNAME</li>
<li>MAIL_PASSWORD</li>
<li>MAIL_ENCRYPTION</li>
<li>MAIL_FROM_ADDRESS</li>
<li>MAIL_FROM_NAME</li>
</ul>

<p>Tworzenie struktury bazy danych z migracji oraz uzupełnienie danymi testowymi odbywa się po wpisaniu komeny php artisan migrate:fresh --seed</p>
<p>W celu zbuildowania plików JS należy uruchomić komendę npm run production</p>
