<?php

use App\Http\Controllers\AnalyticsController;
use App\Http\Controllers\AutomationController;
use App\Http\Controllers\Lists\LeedsController;
use App\Http\Controllers\MailingListController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\UserController;
use App\Models\MailingList;
use App\Models\Tag;
use App\Models\Trigger;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
    Route::resource('lists', MailingListController::class);
    Route::resource('lists.leeds', LeedsController::class);
    Route::resource('automation', AutomationController::class);;
    Route::resource('tags', TagsController::class);
    Route::get('user', [UserController::class, 'edit'])->name('user.edit');
    Route::patch('user', [UserController::class, 'update'])->name('user.update');
});

Route::post('an', [AnalyticsController::class, 'report'])->name('analytics.report');
