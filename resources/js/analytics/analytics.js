const urlParams = new URL(document.currentScript.src);
const id = urlParams.searchParams.get('id');
const reportUrl = process.env.MIX_REPORT_URL;

const report = function(event) {
    value = (event.target.value) ? event.target.value : '';
    var xhr = new XMLHttpRequest();
    xhr.open('POST', reportUrl, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        an_key: getCookie('an_key'),
        id: id,

        event: {
            type: event.type,
            id: event.target.id,
            className: event.target.className,
            tagName: event.target.tagName,
            timeStamp: event.timeStamp,
            value: value,
            baseURI: event.target.baseURI
        }
    }));
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

const cookie = getCookie('an_key');
if (cookie.length == 0) {
    setCookie('an_key', Math.random().toString(36).substring(1), 356);
}


const events = ['click', 'load', 'change'];
for (let event of events) {
    window.addEventListener(event, report)
}
