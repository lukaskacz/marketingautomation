import getBlockType from "./get-block-type";
import getDescription from "./get-description";

export default function updateBlock(e) {
    const div = $(this).closest('.settings');
    const id = div.attr('data-id');

    const type = getBlockType(div);
    const data = div.find('form').serializeArray();
    const formatedData = {};

    for (let i of data) {
        Object.assign(formatedData, {
            [i.name]: i.value
        });
    }
    div.attr('data-params', JSON.stringify(formatedData));
    const block = $(".canvas").find(`[data-id=${id}]`);
    block.attr('data-params', JSON.stringify(formatedData));
    const description = block.find('.description').first();
    description.html(getDescription(type, formatedData));

}
