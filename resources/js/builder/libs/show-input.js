export default function showInput(type, name, value) {
    const input = $(`.settings.${type} [name='${name}']`);
    const div = input.closest('.form-group');
    if (value) { input.val(value); }
    div.removeClass('hidden');
}
