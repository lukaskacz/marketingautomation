import getDescription from './get-description';
import blockClick from './block-click';
import removeBlock from './remove-block';
import showModal from './show-modal';

export default function getBlock(blockType, localId, data) {
    let blockTypeName = "";
    const id = window.localStorage.getItem('parent-id');
    if (blockType == 'action') blockTypeName = "To:"
    if (blockType == 'trigger') blockTypeName = "Wyzwalacz"
    if (blockType == 'condition') blockTypeName = "Jeżeli:"
    const block = $(`<div class="block" data-id="${blockType}_${localId}" data-params='${JSON.stringify(data)}'>
    </div>`);
    const title = $(`<div class="title">${blockTypeName}</div>`);
    const description = $(`<div class="description">${getDescription(blockType,data)}</div>`);

    block.append(title).append(description);

    if (blockType != 'trigger') {
        block.append('<div class="line top"></div>');
        block.append(`<button class="remove btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>`)
    }
    block.addClass(blockType);

    if (blockType == 'trigger') {
        block.append($(`<div class="line end">
        <button class="add-button"><i class="fas fa-plus"></i></button>
        </div>
       `))
    }
    if (blockType == 'condition') {
        block.append($(`<div class="line success">
        <div class="line horizontal">
        <div class="line success end">
        <button class="add-button"><i class="fas fa-plus"></i></button>
        </div>
        </div>
        </div>`))

        block.append($(`<div class="line fail">
        <div class="line horizontal end">
        <div class="line fail end">
        <button class="add-button"><i class="fas fa-plus"></i></button>
        </div>
        </div>
        </div>`))

        const canvas = $(".canvas");
        const canvasWidth = canvas.width();
        const parnetBlock = $(`.block[data-id='${id}']`);

        let parentsCount = parnetBlock.parents(".condition").length;
        if (parnetBlock.hasClass("condition")) {
            parentsCount++;
        }

        const width = canvasWidth / (parentsCount + 1) / 4;
        block.find('.line.horizontal').css({ width: width + 'px' });

    }
    if (blockType == 'action') {
        block.append($(`<div class="line end">
        <button class="add-button"><i class="fas fa-plus"></i></button>
        </div>`))
    }
    block.click(blockClick);
    block.find(".remove").click(removeBlock);
    block.find('.add-button').click(showModal);


    return block;
};
