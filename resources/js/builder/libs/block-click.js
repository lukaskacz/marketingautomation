import getBlockType from "./get-block-type";

export default function blockClick(e) {
    e.stopPropagation();
    e.preventDefault();
    $(".sidebar .form-group").addClass('hidden').val('');
    $(".settings").addClass('hidden');
    const data = $(this).attr('data-params');
    const id = $(this).attr('data-id');
    let type = getBlockType($(this));

    $(`.settings.${type}`).removeClass('hidden').attr('data-params', data).attr('data-id', id);
    $(`.settings.${type} [name="type"]`).val(JSON.parse(data).type).change();

}
