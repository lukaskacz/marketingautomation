export default function getBlockType(div) {
    if (div.hasClass('condition')) return 'condition';
    if (div.hasClass('action')) return 'action';
    if (div.hasClass('trigger')) return 'trigger';

}
