export default function showModal(e) {
    let conditionType = '';

    if ($(this).closest('.line').hasClass('success')) conditionType = 'success';
    if ($(this).closest('.line').hasClass('fail')) conditionType = 'fail';
    const id = $(this).closest(".block").attr('data-id');
    window.localStorage.setItem('parent-id', id);
    window.localStorage.setItem('condition-type', conditionType);
    $('#modal').modal('show');
}
