import appendBlock from './append-block';
import getBlock from './get-block';


export default function render(sequence, parent = null, condition = null) {

    if (sequence) {
        if (sequence.block_type == 'trigger') {
            const block = getBlock('trigger', sequence.id, sequence);
            appendBlock(parent, block, condition);
            render(sequence.next, block);
        }

        if (sequence.block_type == 'condition') {
            const block = getBlock('condition', sequence.id, sequence)
            appendBlock(parent, block, condition);
            render(sequence.success_next, block, 'success');
            render(sequence.fail_next, block, 'fail');
        }

        if (sequence.block_type == 'action') {
            const block = getBlock('action', sequence.id, sequence)

            appendBlock(parent, block, condition);
            render(sequence.next, block);

        }
    }
}
