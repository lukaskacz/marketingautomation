import appendBlock from './append-block';
import getBlock from './get-block';
import randomId from './random-id';

export default function addBlock(e) {
    const id = window.localStorage.getItem('parent-id');
    const blockType = $(this).attr('data-block-type');
    const block = getBlock(blockType, 'new_' + randomId(), {});
    const parnetBlock = $(`.block[data-id='${id}']`);
    const conditionType = window.localStorage.getItem('condition-type');
    appendBlock(parnetBlock, block, conditionType);

    $("#modal").modal('hide');
}
