import showInput from './show-input';

export default function inputChange(e) {
    const value = $(this).val();
    const settingsDiv = $(this).closest('.settings');
    const data = JSON.parse(settingsDiv.attr('data-params'));

    settingsDiv.find('.form-group').addClass('hidden');

    if (settingsDiv.hasClass('trigger')) {
        showInput('trigger', 'type');
        if (value == 'load') {
            showInput('trigger', 'url', data.url);
            showInput('trigger', 'time', data.time);
        }
        if (value == 'click' || value == 'change') {
            showInput('trigger', 'url', data.url);
            showInput('trigger', 'element_id', data.element_id);
            showInput('trigger', 'element_tag', data.element_tag);
            showInput('trigger', 'element_class', data.element_class);
        }
        if (value == 'subscribe') {
            showInput('trigger', 'mailing_list_id', data.mailing_list_id);
        }

        if (value == 'tag') {
            showInput('trigger', 'tag_id', data.tag_id);
        }
    }
    if (settingsDiv.hasClass('condition')) {
        showInput('condition', 'type');
        if (value == 'load') {
            showInput('condition', 'url', data.url);
            showInput('condition', 'time', data.time);
        }
        if (value == 'click' || value == 'change') {

            showInput('condition', 'url', data.url);
            showInput('condition', 'element_id', data.element_id);
            showInput('condition', 'element_tag', data.element_tag);
            showInput('condition', 'element_class', data.element_class);
        }
        if (value == 'subscribe' || value == 'unsubscribe') {
            showInput('condition', 'mailing_list_id', data.mailing_list_id);
        }

        if (value == 'tag' || value == 'remove_tag') {
            showInput('condition', 'tag_id', data.tag_id);
        }
    }
    if (settingsDiv.hasClass('action')) {
        showInput('action', 'type');
        if (value == 'tag' || value == 'remove_tag') {
            showInput('action', 'tag_id', data.tag_id);
        }
        if (value == 'subscribe' || value == 'unsubscribe') {
            showInput('action', 'mailing_list_id', data.mailing_list_id);
        }
        if (value == 'update') {
            showInput('action', 'field', data.field)
            showInput('action', 'url', data.url)
            showInput('action', 'element_tag', data.element_tag)
            showInput('action', 'element_id', data.element_id)
            showInput('action', 'element_class', data.element_class)
        }

        if (value == 'wait') {
            showInput('action', 'interval', data.interval)
            showInput('action', 'interval_type', data.interval_type)
        }

        if (value == 'send') {
            showInput('action', 'subject', data.subject)
            showInput('action', 'content', data.content)
        }




    }
}
