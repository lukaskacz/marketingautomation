import Axios from "axios";
import swal from "sweetalert";


export default function removeSchema() {
    swal({
        title: "Czy jesteś pewien?",
        text: "Po usunięciu tego schematu przepadnie on bezpowrotnie. Listy leedów pozostaną w obecnym stanie.",
        icon: "info",
        buttons: {
            cancel: "Anuluj",
            ok: "Tak"
        },
    }).then(data => {
        if (data == 'ok') {
            const id = window.automation.id;
            Axios.delete('/automation/' + id).then(response => {
                document.location.href = "/";
            });
        }
    })

}
