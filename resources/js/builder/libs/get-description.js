export default function getDescription(group, data) {
    let label = "";
    const labels = {
        load: 'odwiedzono',
        click: 'kliknięcie',
        change: 'zmieniono wartość',
        subscribe: 'zapisano na',
        tag: 'przypisano'
    }

    if (group == 'trigger' || group == 'condition') {
        label = labels[data.type];
        if (data.type == 'load') {
            let url = "dowolną stronę";
            if (data.url) {
                url = "stronę, której adres zawiera ciąg \"" + data.url + "\""
            }
            let time = ""
            if (data.time) {
                time = `i przebywano na niej przez ${data.time} sekund`;
            }

            label += ` ${url} ${time}`
        }
        if (data.type == 'click') {
            let url = "na dowolnej stronie";
            if (data.url) {
                url = "na stronie, której adres zawiera ciąg \"" + data.url + "\""
            }
            let element = '';

            if (data.element_tag) {
                element += data.element_tag;
            }
            if (data.element_id) {
                element += '#' + data.element_id;
            }
            if (data.element_class) {
                element += '.' + data.element_class;
            }
            if (element.length) {
                element = `"${element}"`
            }

            if (!element.length) {
                element = "dowolnego elementu";
            }
            label += ` ${element} ${url}`
        }

        if (data.type == 'change') {
            let url = "na dowolnej stronie";
            if (data.url) {
                url = "na stronie, której adres zawiera ciąg \"" + data.url + "\""
            }
            let element = '';

            if (data.element_tag) {
                element += data.element_tag;
            }
            if (data.element_id) {
                element += data.element_id;
            }
            if (data.element_class) {
                element += data.element_class;
            }

            if (!element.length) {
                element = "dowolnego pola formularza";
            }
            label += ` ${element} ${url}`
        }
        if (data.type == 'subscribe') {
            if (data.mailing_list_id) {
                const list = window.lists.find(l => l.id == data.mailing_list_id);
                label += ` listę "${list.name}"`
            } else {
                label += ` dowolną listę`
            }
        }

        if (data.type == 'tag') {
            if (data.tag_id) {
                const tag = window.tags.find(t => t.id == data.tag_id);
                label += ` tag "${tag.name}"`
            } else {
                label += ` dowolny tag`
            }
        }

    }
    const actionsLabels = {
        send: 'wyślij maila',
        subscribe: 'zapisz na listę',
        tag: 'przypisz tag',
        remove_tag: 'usuń tag',
        unsubscribe: 'wypisz z listy',
        wait: 'czekaj',
        update: 'zaktualizuj wartość pola'
    }
    const customFieldsLabels = {
        email: 'Email',
        name: 'Imię i nazwisko',
        custom_field_1: 'Dodatkowe pole 1',
        custom_field_2: 'Dodatkowe pole 2',
        custom_field_3: 'Dodatkowe pole 3',
        custom_field_4: 'Dodatkowe pole 4'
    }
    if (group == 'action') {
        label = actionsLabels[data.type];
        if (data.type == 'subscribe' || data.type == 'unsubscribe') {
            const list = window.lists.find(l => l.id == data.mailing_list_id);

            label += ` "${list.name}"`;
        }
        if (data.type == 'tag' || data.type == 'remove_tag') {
            const tag = window.tags.find(t => t.id == data.tag_id);
            label += ` "${tag.name}"`;
        }

        if (data.type == 'update') {

            let element = '';

            if (data.element_tag) {
                element += data.element_tag;
            }
            if (data.element_id) {
                element += '#' + data.element_id;
            }
            if (data.element_class) {
                element += '.' + data.element_class;
            }
            if (element.length) {
                element = `"${element}"`
            }

            if (!element.length) {
                element = "dowolnego elementu";
            }

            let url = "znajdującego się na dowolnej stronie";
            if (data.url) {
                url = "znajdującego się na stronie, której adres zawiera ciąg \"" + data.url + "\""
            }

            label = `Pobierz wartość z pola ${element} ${url}`


            label += ` i zapisz ją do pola "${customFieldsLabels[data.field]}"`;
        }
    }

    if (!label) {
        label = "";
    }
    return label;
}
