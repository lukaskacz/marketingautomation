export default function removeBlock(e) {
    if (confirm("Czy jesteś pewien, że chcesz usunąć ten blok?")) {
        $(this).closest('.line').find('.add-button').prop('disabled', false);
        $(this).closest('.block').remove();
    }

}
