import getBlockType from './get-block-type';

export default function getTree(element = null) {
    const type = (element) ? getBlockType(element) : 'trigger';
    element = (element) ? element : $(".canvas").find('.trigger');

    const data = JSON.parse(element.attr('data-params'));
    Object.assign(data, { block_type: type, id: element.attr('data-id') });
    if (type == 'condition') {
        const nextSuccess = element.find('.success.end .block').first();
        const nextFail = element.find('.fail.end .block').first();

        if (nextSuccess.length) {
            const nextSuccessData = getTree(nextSuccess);
            Object.assign(data, { success_next: nextSuccessData });
        }
        if (nextFail.length) {
            const nextFailData = getTree(nextFail);
            Object.assign(data, { fail_next: nextFailData });
        }
    } else {
        const next = element.find('.line.end .block').first();

        if (next.length) {
            const nextData = getTree(next);
            Object.assign(data, { next: nextData });
        }
    }

    return data;
}
