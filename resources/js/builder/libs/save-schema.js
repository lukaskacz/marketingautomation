import Axios from "axios";
import getTree from "./get-tree";
import swal from 'sweetalert';

export default function saveSchema() {

    const name = $(".schema-name").val();
    const data = ({ name: name, schema: getTree() });

    const success = function(data) {
        const id = data.data.id;

        swal("Hurra!", "Twój schemat automatyzacji został zapisany!", "success").then(() => {
            document.location.href = '/automation/' + id + '/edit'
        })
    }

    const fail = function() {
        swal("Oops!", "Coś poszło nie tak..", "error")
    }
    if (window.automation) {
        Axios.patch('/automation/' + automation.id, data).then(success, fail);
    } else {
        Axios.post('/automation', data).then(success, fail);
    }
}
