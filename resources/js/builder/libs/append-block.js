export default function appendBlock(parent, block, condition) {
    if (parent) {
        if (parent.hasClass("trigger")) {
            parent.find('.end').append(block);
            parent.find('> .end > .add-button').prop('disabled', true);
        }
        if (parent.hasClass("condition")) {
            if (condition == 'fail') {
                parent.find('> .line > .line > .fail.end').append(block);
                parent.find('> .line > .line > .fail.end > .add-button').prop('disabled', true);
            }
            if (condition == 'success') {
                parent.find('> .line > .line > .success.end').append(block);
                parent.find('> .line > .line > .success.end > .add-button').prop('disabled', true);
            }
        }
        if (parent.hasClass('action')) {
            parent.find('.end').append(block);
            parent.find('> .end > .add-button').prop('disabled', true);
        }

    } else {
        block.css({ left: 'calc( 50% - 100px )' });
        $(".canvas").append(block);
    }
    if (block.hasClass('condition')) {
        let parentsCount = block.parents('.block.condition').length;
        const canvasWidth = $(".canvas").width();
        const width = canvasWidth / (parentsCount + 1) / 3;
        block.find('.line.horizontal').css({ width: width + 'px' })
    }

}
