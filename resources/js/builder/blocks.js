import inputChange from './libs/input-change';
import updateBlock from './libs/update-block';
import render from './libs/render';
import addBlock from './libs/add-block';
import saveSchema from './libs/save-schema';
import getBlock from "./libs/get-block";
import appendBlock from "./libs/append-block";
import randomId from './libs/random-id';
import removeSchema from './libs/remove-schema';


$(document).ready(function() {

    $(".settings [name='type']").change(inputChange);
    $(".settings .form-control").change(updateBlock).keyup(updateBlock);
    $("#modal .block-type-button").click(addBlock);
    $("#save").click(saveSchema);
    $("#delete").click(removeSchema);

    if (window.sequence) {
        render(window.sequence);
    } else {
        const block = getBlock('trigger', randomId(), {});
        appendBlock(null, block);
    }


});
