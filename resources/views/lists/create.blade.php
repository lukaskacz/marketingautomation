@extends('adminlte::page')

@section('title', 'Nowa lista leedów')

@section('content_header')
<h1>Nowa lista leedów</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        {{Form::open(['url'=>route('lists.store')])}}
        @include('lists.form')
        <button class="btn btn-primary">Zapisz</button>
        {{Form::close()}}
    </div>
</div>
@endsection
