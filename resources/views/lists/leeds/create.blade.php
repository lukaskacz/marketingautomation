@extends('adminlte::page')

@section('title', 'Dodaj nowy kontakt do listy "'.$list->name.'"')

@section('content_header')
<h1>Dodaj nowy kontakt do listy "{{$list->name}}"</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        {{Form::open(['url'=>route('lists.leeds.store',$list->id)])}}
        @include('lists.leeds.form')
        <button class="btn btn-primary">Zapisz</button>
        {{Form::close()}}
    </div>
</div>
@endsection
