@extends('adminlte::page')

@section('title', 'Lista mailingowa "'.$list->name.'"')

@section('content_header')
<h1>Lista mailingowa "{{$list->name}}"</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="col-md-12 text-right mb-2">
            {{Form::open(['url'=>route('lists.destroy',$list->id),'method'=>"DELETE"])}}
            <a href="{{route('lists.leeds.create',$list->id)}}" class="btn btn-success">Dodaj nowy kontakt</a>
            <button onclick="return confirm('Jesteś pewien?')" class="btn btn-danger">Usuń tę listę</button>
            {{Form::close()}}
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Imię i nazwisko</th>
                    <th>Dodatkowe pole 1</th>
                    <th>Dodatkowe pole 2</th>
                    <th>Dodatkowe pole 3</th>
                    <th>Dodatkowe pole 4</th>
                    <th>Tagi</th>
                    <th>Akcje</th>
                </tr>

            </thead>
            <tbody>
                @foreach($leeds as $leed)
                <tr>
                    <td>
                        {{$leed->email}}
                    </td>
                    <td>
                        {{$leed->name}}
                    </td>
                    <td>
                        {{$leed->custom_field_1}}
                    </td>
                    <td>
                        {{$leed->custom_field_2}}
                    </td>
                    <td>
                        {{$leed->custom_field_3}}
                    </td>
                    <td>
                        {{$leed->custom_field_4}}
                    </td>

                    <td>
                        @foreach($leed->tags as $tag)
                        <span class="badge bg-success">{{$tag->name}}</span>
                        @endforeach
                    </td>
                    <td>
                        <a href="{{route('lists.leeds.edit',[$list->id,$leed->id])}}"
                            class="btn btn-xs btn-primary">Edytuj</a>
                        {{Form::open(['method'=>'DELETE','url'=>route('lists.leeds.destroy',[$list->id,$leed->id]),'class'=>'inline'])}}
                        <button class="btn btn-danger btn-xs" onclick="return confirm('Jesteś pewien?')">Usuń</button>
                        {{Form::close()}}
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12">
                {{$leeds->links()}}
            </div>
        </div>

    </div>
</div>

@stop

