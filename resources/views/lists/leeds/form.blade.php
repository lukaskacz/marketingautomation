<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            {!! Form::label('email','Adres email', ['class' => 'col-md-12']) !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('name','Imię i nazwisko', ['class' => 'col-md-12']) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('tags') ? 'has-error' : ''}}">
            {!! Form::label('tags','Tagi', ['class' => 'col-md-12']) !!}
            {!! Form::select('tags[]', $tags->pluck('name','id'), null,
            ['multiple'=>true,'class' =>
            'form-control'])
            !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('custom_field_1') ? 'has-error' : ''}}">
            {!! Form::label('custom_field_1','Dodatkowe pole 1', ['class' => 'col-md-12']) !!}
            {!! Form::text('custom_field_1', null, ['class' => 'form-control']) !!}
            {!! $errors->first('custom_field_1', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('custom_field_2') ? 'has-error' : ''}}">
            {!! Form::label('custom_field_2','Dodatkowe pole 2', ['class' => 'col-md-12']) !!}
            {!! Form::text('custom_field_2', null, ['class' => 'form-control']) !!}
            {!! $errors->first('custom_field_2', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('custom_field_3') ? 'has-error' : ''}}">
            {!! Form::label('custom_field_3','Dodatkowe pole 3', ['class' => 'col-md-12']) !!}
            {!! Form::text('custom_field_3', null, ['class' => 'form-control']) !!}
            {!! $errors->first('custom_field_3', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('custom_field_4') ? 'has-error' : ''}}">
            {!! Form::label('custom_field_4','Dodatkowe pole 4', ['class' => 'col-md-12']) !!}
            {!! Form::text('custom_field_4', null, ['class' => 'form-control']) !!}
            {!! $errors->first('custom_field_4', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
