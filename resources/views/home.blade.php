@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Instrukcja instalacji
            </div>
            <div class="card-body">

                <p class="card-text">Aby zintegrować swoją stronę z systemem należy umieścić w sekcji
                    <strong>HEAD</strong> strony kodu śledzącego:</p>
                <p class="card-text">
                    <code>
                          {{ '<stript src="'.url('an.js').'?id='.auth()->user()->id.'"></script>' }}
                      </code>
                </p>
                <p class="card-text">Kod ten jest niezbędny do poprawnego działania systemu. Zbiera on informacje o
                    zachowaniu użytkowników, takie jak odwiedzone strony, czas spędzony w podstronie, kliknięte elementy
                    i zmienione wartości pól formularzy.
                </p>
                <p>
                    Kod ten można umieścić bezpośrednio w treści strony lub przy użyciu Google Tag Managera.</p>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Statystyki z ostatnich 7 dni
            </div>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <td>Ilość odebranych zdarzeń</td>
                        <td>{{$eventsCount}}</td>
                    </tr>
                    <tr>
                        <td>Wykonanych akcji</td>
                        <td>{{$actionsCount}}</td>
                    </tr>
                    <tr>
                        <td>Nowych leedów</td>
                        <td>{{$leeds}}</td>
                    </tr>
                    <tr>
                        <td>Nowych leedów z adresem email</td>
                        <td>{{$leedsWithEmail}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
