@extends('adminlte::page')

@section('title', 'Schemat automatyzacji')



@section('content')
<div class="card">
    <div class="card-body">
        @include('builder');
    </div>
</div>
@endsection

@section('js')
<script>
    window.automation = {!! $automation !!};
    window.sequence = {!! $sequence !!};
    window.lists = {!! $lists !!};
    window.tags = {!! $tags !!};
</script>

@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('builder.css') }}">
@endsection
