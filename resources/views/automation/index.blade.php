@extends('adminlte::page')

@section('title', 'Automatyzacje')

@section('content_header')
<h1>Lista schematów automatyzacji</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th>Akcje</th>
                </tr>

            </thead>
            <tbody>
                @foreach($triggers as $trigger)
                <tr>
                    <td>
                        {{$trigger->id}}
                    </td>
                    <td>
                        {{$trigger->name}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12">
                {{$triggers->links()}}
            </div>
        </div>

    </div>
</div>

@stop

