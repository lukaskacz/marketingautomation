@extends('adminlte::page')

@section('title', 'Stwórz schemat automatyzacji')



@section('content')
<div class="card">
    <div class="card-body">
        @include('builder');
    </div>
</div>
@endsection

@section('js')
<script>
    window.lists = {!! $lists !!};
    window.tags = {!! $tags !!};
</script>
<script src="{{asset('builder.js')}}"></script>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('builder.css') }}">
@endsection
