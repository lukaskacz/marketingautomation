@extends('adminlte::page')

@section('title', 'Nowa lista leedów')

@section('content_header')
<h1>Edycja danych osobowych</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        {{Form::model($user,['url'=>route('user.update'),'method'=>'PATCH'])}}
        @include('user.form')
        <button class="btn btn-primary">Zapisz</button>
        {{Form::close()}}
    </div>
</div>
@endsection
