@extends('adminlte::page')

@section('title', 'Automatyzacje')

@section('content_header')
<h1>Lista schematów automatyzacji</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">

        <a href="{{route('tags.create')}}" class="btn btn-success btn-xs">Dodaj nowy tag</a>

        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th>Akcje</th>
                </tr>

            </thead>
            <tbody>
                @foreach($tags as $tag)
                <tr>
                    <td>
                        {{$tag->id}}
                    </td>
                    <td>
                        {{$tag->name}}
                    </td>
                    <td>
                        {{Form::open(['method'=>'DELETE','url'=>route('tags.destroy',[$tag->id]),'class'=>'inline'])}}
                        <button class="btn btn-danger btn-xs"
                            onclick="return confirm('Jesteś pewien? Ta akcja może spowodować uruchomienie niektórych automatyzacji, które zawierają wyzwalacz lub warunek \'Usunięto tag\'')">Usuń</button>
                        {{Form::close()}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12">
                {{$tags->links()}}
            </div>
        </div>

    </div>
</div>

@stop
