<div class="container-fluid">
    <div class="row">
        <div class="col-md-9 canvas py-4">
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    Ustawienia
                </div>
                <div class="card-body">
                    <div class="settings trigger hidden" data-params="{}">
                        <form>
                            <h5>Edytujesz wyzwalacz</h5>
                            <div class="form-group">
                                <label>Zdarzenie:</label>
                                <select name="type" class="form-control">
                                    <option value="load">Odwiedziny</option>
                                    <option value="click">Kliknięcie</option>
                                    <option value="change">Zmiana wartości pola w formularzu</option>
                                    <option value="subscribe">Zapisanie na listę</option>
                                    <option value="tag">Przypisanie taga</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Użytkownik spędził na stronie minimum:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="time">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sekund</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Lista:</label>
                                <select name="mailing_list_id" class="form-control">
                                    <option value="">Dowolna lista</option>
                                    @foreach($lists as $list)
                                    <option value="{{$list->id}}">{{$list->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tagi:</label>
                                <select name="tag_id" class="form-control">
                                    @foreach($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Link do strony na której miało miejsce zdarzenie zawiera:</label>
                                <input type="text" class="form-control" name="url">
                            </div>
                            <div class="form-group">
                                <label for="">Tag HTML elementu:</label>
                                <select name="element_tag" class="form-control">
                                    <option value="">Dowolny</option>
                                    <option value="DIV">DIV</option>
                                    <option value="P">P</option>
                                    <option value="BUTTON">BUTTON</option>
                                    <option value="INPUT">INPUT</option>
                                    <option value="TEXTAREA">TEXTAREA</option>
                                    <option value="IMG">IMG</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">ID elementu:</label>
                                <input type="text" class="form-control" name="element_id">
                            </div>
                            <div class="form-group">
                                <label for="">Klasa elementu:</label>
                                <input type="text" class="form-control" name="element_class">
                            </div>
                        </form>
                    </div>

                    <div class="settings condition hidden" data-params="{}">
                        <form>
                            <h5>Edytujesz warunek</h5>
                            <div class="form-group">
                                <label>Zdarzenie:</label>
                                <select name="type" class="form-control">
                                    <option value="load">Odwiedziny</option>
                                    <option value="click">Kliknięcie</option>
                                    <option value="change">Zmiana wartości pola w formularzu</option>
                                    <option value="subscribe">Zapisanie na listę</option>
                                    <option value="tag">Przypisanie taga</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Użytkownik spędził na stronie minimum:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="time">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sekund</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Link do strony na której miało miejsce zdarzenie zawiera:</label>
                                <input type="text" class="form-control" name="url">
                            </div>
                            <div class="form-group">
                                <label for="">Lista:</label>
                                <select name="mailing_list_id" class="form-control">
                                    <option value="">Dowolna lista</option>
                                    @foreach($lists as $list)
                                    <option value="{{$list->id}}">{{$list->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tagi:</label>
                                <select name="tag_id" class="form-control">
                                    @foreach($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tag HTML elementu:</label>
                                <select name="element_tag" class="form-control">
                                    <option value="">Dowolny</option>
                                    <option value="DIV">DIV</option>
                                    <option value="P">P</option>
                                    <option value="BUTTON">BUTTON</option>
                                    <option value="INPUT">INPUT</option>
                                    <option value="TEXTAREA">TEXTAREA</option>
                                    <option value="IMG">IMG</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">ID elementu:</label>
                                <input type="text" class="form-control" name="element_id">
                            </div>
                            <div class="form-group">
                                <label for="">Klasa elementu:</label>
                                <input type="text" class="form-control" name="element_class">
                            </div>
                        </form>
                    </div>

                    <div class="settings action hidden" data-params="{}">
                        <form>
                            <h5>Edytujesz Akcję</h5>
                            <div class="form-group">
                                <label>Zdarzenie:</label>
                                <select name="type" class="form-control">
                                    <option value="subscribe">Zapisz na listę</option>
                                    <option value="unsubscribe">Usuń z listy</option>
                                    <option value="tag">Przypisz tag</option>
                                    <option value="remove_tag">Usuń tag</option>
                                    <option value="update">Zaktualizuj wartość pola</option>
                                    <option value="wait">Czekaj</option>
                                    <option value="send">Wyślij wiadomość</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Jak długo:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="interval">
                                    <div class="input-group-append">
                                        <select name="interval_type" class="form-control input-group-text">
                                            <option value="minute">Minut</option>
                                            <option value="hours">Godzin</option>
                                            <option value="days">Dni</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Lista:</label>
                                <select name="mailing_list_id" class="form-control">

                                    @foreach($lists as $list)
                                    <option value="{{$list->id}}">{{$list->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Link do strony na której miało miejsce zdarzenie zawiera:</label>
                                <input type="text" class="form-control" name="url">
                            </div>
                            <div class="form-group">
                                <label for="">Tag HTML elementu:</label>
                                <select name="element_tag" class="form-control">
                                    <option value="">Dowolny</option>
                                    <option value="DIV">DIV</option>
                                    <option value="P">P</option>
                                    <option value="BUTTON">BUTTON</option>
                                    <option value="INPUT">INPUT</option>
                                    <option value="TEXTAREA">TEXTAREA</option>
                                    <option value="IMG">IMG</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">ID elementu:</label>
                                <input type="text" class="form-control" name="element_id">
                            </div>
                            <div class="form-group">
                                <label for="">Klasa elementu:</label>
                                <input type="text" class="form-control" name="element_class">
                            </div>
                            <div class="form-group">
                                <label for="">Tag:</label>
                                <select name="tag_id" class="form-control">
                                    @foreach($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="">Tytuł:</label>
                                <input type="text" name="subject" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Treść:</label>
                                <textarea type="text" name="content" class="form-control" rows="6"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Pole:</label>
                                <select name="field" class="form-control">
                                    <option value="email">Email</option>
                                    <option value="name">Imię i nazwisko</option>
                                    <option value="custom_field_1">Dodatkowe pole 1</option>
                                    <option value="custom_field_2">Dodatkowe pole 2</option>
                                    <option value="custom_field_3">Dodatkowe pole 3</option>
                                    <option value="custom_field_4">Dodatkowe pole 4</option>
                                </select>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
            <div class=" mt-5">
                <hr>
                <div class="form-group">
                    <label>Nazwa schematu</label>
                    <input type="text" name="schema-name" class="form-control schema-name"
                        value="{{ $automation->name ?? 'Nazwa schematu' }}">
                </div>
                <div>
                    <button class="btn btn-primary col" id="save">
                        Zapisz schemat
                    </button>
                </div>
                @if(isset($automation))
                <div class="mt-2">
                    <button class="btn btn-danger col" id="delete">
                        Usuń schemat
                    </button>
                </div>
                @endif

            </div>

        </div>
    </div>
</div>
<div class="modal fade " id="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Dodaj blok</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row text-center">
                    <div class="col">
                        <button class="btn btn-warning btn-lg block-type-button"
                            data-block-type="condition">Warunek</button>

                    </div>
                    <div class="col">
                        <button class="btn btn-success btn-lg block-type-button" data-block-type="action">Akcja</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>

            </div>
        </div>
    </div>
</div>
