const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['node_modules/bootstrap/dist/js/bootstrap.js', 'resources/js/analytics/analytics.js'], 'public/an.js')
    .js('resources/js/builder/builder.js', 'public/builder.js')
    .sass('resources/sass/builder.scss', 'public/builder.css')
    .browserSync({
        proxy: 'localhost.com:8000'
    });